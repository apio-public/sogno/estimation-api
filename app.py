from flask import Flask, request, jsonify, make_response, json
from werkzeug.utils import secure_filename
import zipfile
from werkzeug.exceptions import HTTPException
import glob
from os import listdir, getcwd, sys, remove, mkdir, path
from os.path import abspath, isfile, join, isdir
import traceback
import cimpy
import numpy as np
from pyvolt import network, measurement, nv_state_estimator, nv_powerflow
from interfaces import villas_node_interface
from pprint import pprint
from decouple import config
from database import write_estimation
import uuid
import time
import paho.mqtt.client as mqtt
import datetime
import random
import copy

cwd = getcwd()

# API setup
app = Flask(__name__)
UPLOAD_FOLDER = cwd + '/uploads/'
ALLOWED_EXTENSIONS = {'xml', 'cim'}
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['MAX_CONTENT_LENGTH'] = 16 * 1000 * 1000


# MQTT Setup
MQTT_HOST = config("MQTT_HOST", default="rabbitmq")

# measurements files
measurement_config_file = abspath(
    join(cwd, r"./configs/Measurement_config2.json"))


# read mapping file and create mapping vectors
cwd = getcwd()
input_mapping_file = abspath(
    join(cwd, r"./configs/villas_node_input_data.conf"))
output_mapping_file = abspath(
    join(cwd, r"./configs/villas_node_output_data.conf"))
input_mapping_vector = villas_node_interface.read_mapping_file(
    input_mapping_file)
output_mapping_vector = villas_node_interface.read_mapping_file(
    output_mapping_file)


def on_connect(client, userdata, flags, rc):
    """
    The callback for when the client receives a CONNACK response from the server.
    """
    if rc == 0:
        print("connected OK with returned code=", rc)
    else:
        print("Bad connection with returned code=", rc)


def connect(client_name, broker_adress, port=1883):
    mqttc = mqtt.Client(client_name, True)
    mqttc.username_pw_set('admin', 'admin')
    mqttc.on_connect = on_connect
    mqttc.on_message = on_message
    mqttc.connect(broker_adress, port)
    mqttc.loop_start()
    time.sleep(4)
    return mqttc


def on_message(client, userdata, msg):
    with app.app_context():
        message = json.loads(msg.payload)
        print("Messaggio ricevuto dal simulatore, ma non è ancora presente la funzione che adatta l'output del simulatore a pyvolt. Work in progress.")
        pprint(message)


mqttc = connect("estimation-api", MQTT_HOST, 1883)
mqttc.subscribe("/simulation")


# Estimation related functions

def add_noise(v):
    p = random.randrange(-7, 7)
    return v + (p*v)/100


class NumpyEncoder(json.JSONEncoder):
    """ Special json encoder for numpy types """

    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)


def clean_upload_folder():
    files = glob.glob(UPLOAD_FOLDER + 'current_model/*')
    for f in files:
        remove(f)


def custom_error(message, status_code):
    return make_response(jsonify(message), status_code)


def get_demo_simulated_data(simulation_id, model_id, time_stamp, sequence, add_entropy=True, simulate_violation=False):
    fixed_data = [110000.0, 0.0, ]
    # We are using a fixed model "N1","N12","N3","N5","N4","N10","N9","N8","N7","N6","N14","N13","N11","N2"
    var_data = [19845.387471125738, -0.03341533984332116,  # N1
                19745.57725119642, -0.03967421660260819,  # N12
                19579.94062640245, -0.04923017901927501,  # N3
                19571.841307984792, -0.04969084903274851,  # N5
                19566.41079359819, -0.05000963356198258,  # N4
                19560.119850062427, -0.05038081908113559,  # N10
                19552.411843629507, -0.05077543144741772,  # N9
                19553.366128799462, -0.050660161449359725,  # N8
                19548.38634695928, -0.05090615675075075,  # N7
                19542.41754402312, -0.051193852351419045,  # N6
                19541.530812508234, -0.051239973246200735,  # N14
                19843.135027277407, -0.028322191181742663,  # N13
                19812.49611713103, -0.028942027045356453,  # N11
                19794.765901055587, -0.029275775628871705]  # N2

    if True == simulate_violation:
        var_data = [19845.387471125738, -0.03341533984332116,  # N1
                    20945.57725119642, -0.03967421660260819,  # N12
                    19579.94062640245, -0.04923017901927501,  # N3
                    19571.841307984792, -0.04969084903274851,  # N5
                    19566.41079359819, -0.05000963356198258,  # N4
                    19560.119850062427, -0.05038081908113559,  # N10
                    19552.411843629507, -0.05077543144741772,  # N9
                    19553.366128799462, -0.050660161449359725,  # N8
                    19548.38634695928, -0.05090615675075075,  # N7
                    19542.41754402312, -0.051193852351419045,  # N6
                    19741.530812508234, -0.051239973246200735,  # N14
                    20943.135027277407, -0.028322191181742663,  # N13
                    19812.49611713103, -0.028942027045356453,  # N11
                    19794.765901055587, -0.029275775628871705]  # N2

    if (add_entropy == True):
        var_data = list(map(lambda x: add_noise(x), var_data))
    return {
        "simulation_id": simulation_id,
        "model_id": model_id,
        "ts": {
            "origin": [
                time_stamp,
                877953686
            ],
            "received": [
                time_stamp,
                877953686
            ]
        },
        "sequence": sequence,
        "data": fixed_data + var_data
    }


def get_demo_simulated_data_interval(simulation_id, model_id, start_time, step=10, iterations=60, add_entropy=True, simulate_violation=False):
    sequence = 0
    output = []
    while sequence <= iterations:
        thistime = start_time + datetime.timedelta(0, step*sequence)
        msg = get_demo_simulated_data(
            simulation_id, model_id, thistime, sequence, add_entropy, simulate_violation)
        output.append(msg)
        sequence = sequence+1
    return output


@app.route('/', methods=["GET"])
def root():
    return jsonify({'name': "state-estimation-api", "version": "0.2"})


# Endpoint to list available CIM Models
@app.route('/models', methods=["GET"])
def list_models():
    model_files = [f for f in listdir(
        UPLOAD_FOLDER) if isdir(join(UPLOAD_FOLDER, f))]
    return jsonify(list(map(lambda x: {"name": x}, model_files)))


# Endpoint to upload a CIM model
@app.route('/model', methods=["POST"])
def add_cim_model():
    clean_upload_folder()
    f = request.files['file']
    model_id = str(uuid.uuid4())
    if "model_id" in request.form:
        model_id = request.form['model_id']
    model_path = UPLOAD_FOLDER + model_id
    if (path.exists(model_path)):
        return {"message": "model_id exists"}, 400

    final_destination = UPLOAD_FOLDER + secure_filename(f.filename)
    f.save(final_destination)
    mkdir(model_path)
    with zipfile.ZipFile(final_destination, "r") as zip_ref:
        zip_ref.extractall(model_path)

    print("Model uploaded to "+model_path)

    return jsonify({"model_id": model_id})


def get_estimation(model_files, uncertainty=0, base_apparent_power=25, sequence=1):
    try:
        # Read cim files and create new network.System object
        res = cimpy.cim_import(model_files, "cgmes_v2_4_15")
        system = network.System()

        system.load_cim_data(res['topology'], base_apparent_power)

        # Execute power flow analysis
        results_pf, num_iter_cim = nv_powerflow.solve(system)

        # --- State Estimation ---
        """ Write here the percent uncertainties of the measurements"""
        V_unc = uncertainty
        I_unc = uncertainty
        Sinj_unc = uncertainty
        S_unc = uncertainty
        Pmu_mag_unc = uncertainty
        Pmu_phase_unc = uncertainty

        # Create measurements data structures
        """use all node voltages as measures"""
        measurements_set = measurement.MeasurementSet()
        for node in results_pf.nodes:
            measurements_set.create_measurement(node.topology_node, measurement.ElemType.Node, measurement.MeasType.Vpmu_mag,
                                                np.absolute(node.voltage_pu), Pmu_mag_unc)
        for node in results_pf.nodes:
            measurements_set.create_measurement(node.topology_node, measurement.ElemType.Node, measurement.MeasType.Vpmu_phase,
                                                np.angle(node.voltage_pu), Pmu_phase_unc)

        measurements_set.meas_creation(dist="uniform", seed=sequence)

        # Perform state estimation
        state_estimation_results = nv_state_estimator.DsseCall(
            system, measurements_set)

        for node in state_estimation_results.nodes:
            print('{}={}'.format(node.topology_node.uuid, node.voltage))

        result = {'branches': [], 'nodes': []}
        for branch in state_estimation_results.branches:
            result['branches'].append({
                'current': np.abs(branch.current),
                'current_phase':  np.angle(branch.current),
                'power_r': np.imag(branch.power),
                'power_a': np.real(branch.power),
                'uuid': branch.topology_branch.uuid
            })

        for node in state_estimation_results.nodes:
            result['nodes'].append({
                'current': np.abs(node.current),
                'current_phase':  np.angle(node.current),
                'voltage': np.abs(node.voltage),
                'voltage_phase':  np.angle(node.voltage),
                'power_r': np.imag(node.power),
                'power_a': np.real(node.power),
                'uuid': node.topology_node.uuid
            })
        return result

    except Exception as e:
        traceback.print_tb(e)
        print(e.__traceback__)
        sys.exit()


def get_demo_state_estimation_results(message, model_files):
    if "sequence" in message:
        sequence = message['sequence']
    else:
        print('Sequence no. not available.')
        sequence = 1
    res = {}
    res = cimpy.cim_import(model_files, "cgmes_v2_4_15")
    system = network.System()
    system.load_cim_data(res['topology'], 25)
    # store the received data in powerflow_results
    powerflow_results = villas_node_interface.receiveVillasNodeInput(
        system, message, input_mapping_vector)

    # read measurements from file
    measurements_set = measurement.MeasurementSet()

    measurements_set.read_measurements_from_file(
        powerflow_results, measurement_config_file)
    scenario_flag = 1

    measurements_set.meas_creation(dist="uniform", seed=sequence)

    # Performs state estimation
    state_estimation_results = nv_state_estimator.DsseCall(
        system, measurements_set)
    print("State estimation completed for sequence="+str(sequence))

    return powerflow_results, state_estimation_results, scenario_flag, sequence


def get_demo_estimation(message, use_working_model=True):
    print("get_demo_estimation() su messaggio", json.dumps(message))

    if "model_id" not in message:
        print("Il messagio non ha il model_id, usciamo")
        return
    if "simulation_id" not in message:
        print("Il messagio non ha il simulation_id, usciamo")
        return

    # Since our models are not working, we provide a hard coded model for the estimation
    model_path = UPLOAD_FOLDER + "working-model"
    if False == use_working_model:
        model_path = UPLOAD_FOLDER + message["model_id"]
        print("Model path: "+model_path)

    if path.exists(model_path) == False:
        return {"message": "model doesn't exist"}, 404

    model_files = [f for f in listdir(
        model_path) if isfile(join(model_path, f))]
    model_files = list(map(lambda f: model_path + "/" + f, model_files))
    try:
        powerflow_results, state_estimation_results, scenario_flag, sequence = get_demo_state_estimation_results(
            message, model_files)
        result = {'branches': [], 'nodes': []}
        for branch in state_estimation_results.branches:
            result['branches'].append({
                'current': np.abs(branch.current),  # * 1000,
                'current_phase':  np.angle(branch.current),
                'power_r': np.imag(branch.power),
                'power_a': np.real(branch.power),
                'uuid': branch.topology_branch.uuid
            })

        for node in state_estimation_results.nodes:
            result['nodes'].append({
                # We want the current as A
                'current': np.abs(node.current),  # * 1000,
                'current_phase':  np.angle(node.current),
                'voltage': np.abs(node.voltage),  # Voltage is in kV
                'voltage_phase':  np.angle(node.voltage),
                'power_r': np.imag(node.power),  # Reactive power is in kVar
                'power_a': np.real(node.power),  # Active power is in MW
                'uuid': node.topology_node.uuid
            })

        # Finished message
        print("Finished state estimation for sequence " + str(sequence))
        #write_estimation(result, model_id, simulation_id)

        return result

    except Exception as e:
        traceback.print_tb(e)
        print(e.__traceback__)
        sys.exit()


@ app.route('/estimation', methods=["POST"])
def run_state_estimation():
    sequence = 1
    uncertainty = 5
    simulation_id = str(uuid.uuid4())
    base_apparent_power = 25  # MW
    if ("base_apparent_power" in request.json):
        base_apparent_power = request.json["base_apparent_power"]

    if ("simulation_id" in request.json):
        simulation_id = request.json["simulation_id"]

    if ("model_id" in request.json):
        model_id = request.json["model_id"]
    else:
        return {"message": "model_id is required"}, 400

    if ("sequence" in request.json):
        sequence = request.json["sequence"]

    model_path = UPLOAD_FOLDER + model_id
    if path.exists(model_path) == False:
        return {"message": "model doesn't exist"}, 404

    model_files = [f for f in listdir(
        model_path) if isfile(join(model_path, f))]
    model_files = list(map(lambda f: model_path + "/" + f, model_files))

    result = get_estimation(model_files, uncertainty=uncertainty,
                            base_apparent_power=base_apparent_power, sequence=sequence)
    return jsonify(result)


@ app.route('/simulation', methods=["POST"])
def run_simulation():
    sequence = 1
    uncertainty = 5
    simulation_id = str(uuid.uuid4())
    step = 30
    iterations = 60
    base_apparent_power = 25  # MW
    if ("base_apparent_power" in request.json):
        base_apparent_power = int(request.json["base_apparent_power"])

    if ("step" in request.json):
        step = int(request.json["step"])
    if ("iterations" in request.json):
        iterations = int(request.json["iterations"])
    start_time = datetime.datetime.now()

    if ("start_time" in request.json):
        start_time = datetime.datetime.fromisoformat(
            request.json["start_time"])

    if ("simulation_id" in request.json):
        simulation_id = request.json["simulation_id"]

    if ("model_id" in request.json):
        model_id = request.json["model_id"]
    else:
        return {"message": "model_id is required"}, 400

    model_path = UPLOAD_FOLDER + model_id
    if path.exists(model_path) == False:
        print("Il model non esiste, usciamo")
        return {"message": "model doesn't exist"}, 404

    model_files = [f for f in listdir(
        model_path) if isfile(join(model_path, f))]
    model_files = list(map(lambda f: model_path + "/" + f, model_files))

    while sequence <= iterations:
        thistime = start_time + datetime.timedelta(0, step*sequence)
        result = get_estimation(model_files, uncertainty=uncertainty,
                                base_apparent_power=base_apparent_power, sequence=sequence)
        write_estimation(estimation=result, model_id=model_id,
                         simulation_id=simulation_id, the_time=thistime)
        sequence = sequence+1
    return {sequence: sequence}, 200


@app.route('/demo/simulation', methods=["POST"])
def run_demo_simulation():
    """
        Here, we generate input data for the estimation. Input data is based on a working model provided by the SOGNO team
        Using our models still doesn't work.

        The generated data is the fed to the estimation algorithm.

        Another method we have is /simulation which will directly simulate the output of the estimator, thus not running any SOGNO algorithm
    """
    simulate_violation = False
    if "simulate_violation" in request.json:
        simulate_violation = request.json["simulate_violation"]

    if False == ("model_id" in request.json):
        return {"message": "missing requirement parameter model_id"}, 400
    model_id = request.json["model_id"]
    simulation_id = str(uuid.uuid4())
    if ("simulation_id" in request.json):
        simulation_id = request.json["simulation_id"]
    now = datetime.datetime.now()

    if "start_time" in request.json:
        print("Using start_time ", request.json["start_time"])
        now = datetime.datetime.fromisoformat(request.json["start_time"])

    use_working_model = True
    if "wm" in request.json:
        print("Using working model is set to:", request.json["wm"])
        use_working_model = request.json["wm"]

    c = 1
    iterations = 60
    step = 10

    if ("iterations" in request.json):
        iterations = int(request.json["iterations"])

    if ("step" in request.json):
        step = int(request.json["step"])

    messages = get_demo_simulated_data_interval(
        simulation_id, model_id, now, step, iterations, False, simulate_violation)

    for msg in messages:
        # Andiamo a lanciare l'estimation
        estimation = get_demo_estimation(msg, use_working_model)
        pprint(estimation)
        time.sleep(0.5)
        write_estimation(estimation, model_id, simulation_id,
                         msg["ts"]["origin"][0])
    return jsonify({"simulation_id": simulation_id})


@ app.errorhandler(HTTPException)
def handle_exception(e):
    """Return JSON instead of HTML for HTTP errors."""
    # start with the correct headers and status code from the error
    response = e.get_response()
    # replace the body with JSON
    response.data = json.dumps({
        "code": e.code,
        "name": e.name,
        "description": e.description,
    })
    response.content_type = "application/json"
    return response


HOST = config("ESTIMATION_API_HOST", default="0.0.0.0")
PORT = config("ESTIMATION_API_PORT", default=5000, cast=int)

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=PORT)
