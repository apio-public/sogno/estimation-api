from influxdb import InfluxDBClient
import datetime
from decouple import config


HOST = config("INFLUX_HOST", default="influxdb")
PORT = config("INFLUX_PORT", default=8086, cast=int)
USERNAME = config("INFLUX_USERNAME", default="telegraf")
PASSWORD = config("INFLUX_PASSWORD", default="telegraf")
DATABASE = config("INFLUX_DATABASE", default="telegraf")
client = InfluxDBClient(host=HOST, port=PORT,
                        username=USERNAME, password=PASSWORD, database=DATABASE)


def write_estimation(estimation, model_id, simulation_id, the_time=datetime.datetime.now().isoformat()):
    to_write = []
    for branch in estimation["branches"]:
        to_write.append({
            "measurement": "estimation",
            "tags": {
                "simulation_id": simulation_id,
                "model_id": model_id,
                "element_id": branch["uuid"]
            },
            "time": the_time,
            "fields": {
                "current": branch["current"],
                "current_phase": branch["current_phase"],
                "power_a": branch["power_a"],
                "power_r": branch["power_r"]
            }
        })

    for node in estimation["nodes"]:
        to_write.append({
            "measurement": "estimation",
            "tags": {
                "simulation_id": simulation_id,
                "model_id": model_id,
                "element_id": node["uuid"]
            },
            "time": the_time,
            "fields": {
                "voltage": node["voltage"],
                "voltage_phase": node["voltage_phase"],
                "current": node["current"],
                "current_phase": node["current_phase"],
                "power_a": node["power_a"],
                "power_r": node["power_r"]
            }
        })

    client.write_points(to_write)
