## Estimation API

Python service that exposes PyVolt functionalities through a JSON HTTP API.

## Get started

### Dependencies

- InfluxDB

### Docker


```bash
cd path/to/this/repo

docker build -t sogno/estimation-api .

docker run -p5000:5000 sogno/estimation-api
```

### Kubernetes (Helm)

Assuming the availability of a docker registry, in this case runing at localhost:5000

```bash
cd path/to/this/repo

# Build the image and push it to the k8s accessible registry
docker build -t localhost:5000/estimation-api:latest . && docker push localhost:5000/estimation-api:latest

# Update values according to your environment

# Run the service through helm
helm install -f helm/values.yaml estimation-api ./helm
```

