FROM python:3.7

LABEL name=estimation-service

WORKDIR /

RUN mkdir -p /configs
RUN mkdir -p /interfaces
RUN mkdir -p /uploads/working-model

COPY requirements.txt .
COPY app.py .
COPY database.py .
COPY configs/* ./configs/
COPY interfaces/* ./interfaces/
COPY uploads/working-model/* ./uploads/working-model/

RUN pip3 install --upgrade pip
RUN pip3 install git+https://github.com/cim-iec/cimpy.git
RUN pip3 install git+https://github.com/RWTH-ACS/pyvolt.git
RUN pip3 install -r requirements.txt
RUN pip3 install flask
RUN pip3 install influxdb
RUN git clone https://github.com/RWTH-ACS/pyvolt.git

ENV PYTHONUNBUFFERED=1

CMD python3 -m flask run --host=0.0.0.0